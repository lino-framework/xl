=======================
Lino Extensions Library
=======================

This is the source code of the ``lino-xl`` Python package, a collection of
plugins used by many Lino applications. It is an integral part of the `Lino
framework <https://www.lino-framework.org>`__, a sustainably free open-source
project maintained by the `Synodalsoft team <https://www.synodalsoft.net>`__.
Your contributions are welcome.

- Developer Guide: https://dev.lino-framework.org
- Code repository: https://gitlab.com/lino-framework/xl
- Test suite: https://gitlab.com/lino-framework/book/-/pipelines
- Maintainer: https://www.synodalsoft.net
- Changelog: https://www.lino-framework.org/changes/
- Contact: https://www.saffre-rumma.net/contact/
