# Copyright 2008-2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""These are the plugins included with Lino XL.

The content of this page is being moved to :doc:`/plugins/index`.

See also :ref:`ug.plugins`.

Contacts management
===================

.. autosummary::
   :toctree:

    countries
    contacts
    phones
    households
    humanlinks
    cv
    dupable_partners

Groupware
=========

.. autosummary::
   :toctree:

    boards
    teams
    lists
    groups
    topics
    deploy
    tickets
    skills
    working
    votes


Calendar functionalities
========================

.. autosummary::
   :toctree:

    cal
    calview
    extensible
    reception
    rooms
    courses


Office functionalities
======================

.. autosummary::
   :toctree:

    notes
    outbox
    postings
    mailbox
    caldav

ERP
===

.. autosummary::
   :toctree:

    products
    orders
    subscriptions

System extensions
=================

.. autosummary::
   :toctree:

    appypod
    excerpts
    stars
    userstats


Miscellaneous
=============

.. autosummary::
   :toctree:

    beid
    polls
    clients
    coachings
    trends

Content management
==================

.. autosummary::
   :toctree:

    blogs

Accounting stuff
================

.. autosummary::
   :toctree:

    b2c
    accounting
    trading
    finan
    tim2lino
    sepa
    invoicing
    vatless
    bevat
    bevats
    eevat
    ana
    sheets

The following plugins are already migrated to full narrative docs
:mod:`lino_xl.lib.vat`

Plugin namespaces
=======================

(See :ref:`plugin_namespaces`).

.. autosummary::
   :toctree:

    statbel

Experimental and not much used
==============================

.. autosummary::
   :toctree:

    concepts
    eid_jslib
    events
    families
    properties
    inspect

"""
