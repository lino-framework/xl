# -*- coding: UTF-8 -*-
# Copyright 2025 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
# Developer docs: https://dev.lino-framework.org/plugins/ibanity.html


# from lino.api import dd, _
# from lino_xl.lib.accounting.roles import LedgerStaff
#
# with_suppliers = dd.get_plugin_setting("ibanity", "with_suppliers", False)
# with_documents = dd.get_plugin_setting("ibanity", "with_documents", False)
# outbound_model = dd.get_plugin_setting("ibanity", "outbound_model", None)
