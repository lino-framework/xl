# -*- coding: UTF-8 -*-
# Copyright 2014-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This package contains the code of the :ref:`xl`.

See :ref:`specs.xl` for the list of plugins.

.. autosummary::
   :toctree:

   lib

"""

import os

__version__ = '25.3.0'

# intersphinx_urls = dict(docs="http://xl.lino-framework.org")
srcref_url = 'https://gitlab.com/lino-framework/xl/blob/master/%s'
# doc_trees = ['docs']
